package ru.lexxzone;

/**
 * @author Alexey Dvoryaninov
 * @since 21/03/2020
 */
public class Main {

    public static void main(String[] args) throws Exception {
        System.out.println("* * * * *   WELCOME TO THE TASKBROKER. IT WORKS LIKE A CALCULATOR.   * * * * *\n");
        TaskBroker taskBroker = new TaskBroker();
        boolean isInCycle;
        do {
            taskBroker.execute();
            isInCycle = taskBroker.executeMore();
            if (isInCycle) {
                System.out.println("\n--- NEXT OPERATION ---\n");
            }
        } while (isInCycle);
        System.out.println("\nIT'S TIME TO EXIT...\n");
        Thread.sleep(1000L);
        System.out.println("SEE YOU NEXT TIME!");
    }
}
