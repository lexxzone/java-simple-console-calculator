package ru.lexxzone;

import java.util.Scanner;

/**
 * @author Alexey Dvoryaninov
 * @since 21/03/2020
 */
public class TaskBroker {
    private static Scanner scanner = new Scanner(System.in);
    private static final double[] numbers = {1.0, 1.0};

    /**
     * Receives info from user and run calculation
     */
    public void execute() throws Exception {
        String userOperationType = chooseType();
        askForNumbers(userOperationType);
        System.out.print("THE ANSWER IS ");
        String result = calculate(userOperationType);
        String[] strResult = result.split("\\.");
        if (strResult[1].matches("0+")) {
            System.out.println(strResult[0]);
        } else {
            System.out.println(result);
        }
    }

    /**
     * Calculates result.
     *
     * @param userOperationType which operation will be applied
     * @return result
     * @throws Exception exception
     */
    private String calculate(String userOperationType) throws Exception {
        String result;
        switch (userOperationType) {
            case "+":
                result = String.valueOf(numbers[0] + numbers[1]);
                break;
            case "-":
                result = String.valueOf(numbers[0] - numbers[1]);
                break;
            case "*":
                result = String.valueOf(numbers[0] * numbers[1]);
                break;
            case "/":
                result = String.valueOf(numbers[0] / numbers[1]);
                break;
            default:
                throw new Exception("Sorry, There is Internal error.");
        }
        return result;
    }

    /**
     * Defines that cycle will be started again or not.
     *
     * @return true if it should start again, otherwise false
     */
    public boolean executeMore() {
        System.out.println("\nENTER ANY SYMBOLS TO CALL NEW TASK OR ENTER 'e' TO EXIT:");
        scanner = new Scanner(System.in);
        String answer = scanner.nextLine();
        return !answer.matches("e");
    }

    /**
     * Requiring first and second numbers from user.
     *
     * @param userOperation operation type
     */
    private void askForNumbers(String userOperation) {
        boolean isNumber;
        for (int i = 0; i < numbers.length; i++) {
            String userNumber;
            do {
                System.out.println(String.format("PLEASE ENTER %d %s", (i + 1), "NUMBER:"));
                userNumber = scanner.nextLine().trim();
                if (i == 1 && userOperation.equals("/") && userNumber.matches("0+")) {
                    System.out.println("WE CAN NOT DIVIDE WITH ZERO. CHOOSE ANOTHER NUMBER.");
                    isNumber = false;
                } else {
                    isNumber = userNumber.matches("\\d*\\.?\\d*");
                    if (!isNumber) {
                        System.out.println(
                                "YOU SHOULD ENTER VALUE IN DOUBLE OR INT FORMAT. FOR EXAMPLE LIKE THESE '8' OR '14.6'."
                        );
                    }
                }
            } while (!isNumber);
            numbers[i] = Double.parseDouble(userNumber);
        }
    }

    /**
     * Defining operation type.
     *
     * @return operation type
     */
    private String chooseType() {
        boolean isOperationAvailable = false;
        String userOperation;
        do {
            System.out.println("PLEASE CHOOSE OPERATION TYPE. AVAILABLE TYPES ARE: +, -, *, /");
            userOperation = scanner.nextLine().strip();
            if (userOperation.matches("[+\\-*/]")) {
                isOperationAvailable = true;
            } else {
                System.out.println("YOU SHOULD ENTER CORRECT TYPE!");
            }
        } while (!isOperationAvailable);
        return userOperation;
    }
}
