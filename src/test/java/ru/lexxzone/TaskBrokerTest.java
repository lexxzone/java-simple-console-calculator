package ru.lexxzone;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

/**
 * Simple test for basic logic in the program.
 *
 * @author Alexey Dvoryaninov
 * @since 19/04/2020
 */
public class TaskBrokerTest {

    private final TaskBroker taskBroker = new TaskBroker();
    
    @ParameterizedTest(name = "#{index}: Operation \"{0}\", expected result \"{1}\", unexpected \"{2}\"")
    @MethodSource("getParamsForTestCalculateMethod")
    @DisplayName("Test for method 'calculate()'")
    void testCalculateMethod(String operation, String expected, String unexpected)
            throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method method = TaskBroker.class.getDeclaredMethod("calculate", String.class);
        method.setAccessible(true);
        String result = (String) method.invoke(taskBroker, operation);
        assertEquals(expected, result);
        assertNotEquals(unexpected, result);
    }

    /**
     * @return params for {@link TaskBrokerTest#testCalculateMethod(String, String, String)}
     */
    private static Stream<Arguments> getParamsForTestCalculateMethod() {
        return Stream.of(
                Arguments.of("+", "2.0", "2"),
                Arguments.of("-", "0.0", "0"),
                Arguments.of("*", "1.0", "1"),
                Arguments.of("/", "1.0", "1")
        );
    }
}
