# Simple Java console calculator #

Here is really simple application - console calculator on Java

### Technologies ###

* Java 11
* Maven
* JUpiter

### How it works ###

Just run this app in Java IDE or in console with compiled class.
After that program will ask you for operation and arguments.
You need to enter it and program will return your result.

### Is this a best solution? ###

Of course no. I'm sure you can write it better. It's just an example.

### Contacts ###

You can write me email to lexxzone@gmail.com but keep in mind that I don't check it every day and your email may be filtered as spam by email service.

Best regards, 
Alexey